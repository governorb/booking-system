# Booking System Projects Setup (Windows)

## How to setup?
Clone the project on your machine.
```shell
git clone https://governorb@bitbucket.org/governorb/booking-system.git
cd booking-system
```

### This Repo consists of MVC FrontEnd Project and .Net Core Web API
- [Booking System FrontEnd](MvcBookingSystem)
- [Booking System Web API](MvcBookingSystemApi)

### Booking System FrontEnd Setup

Change directory and follow instructions in the ReadMe
```shell
cd MvcBookingSystem
```

### Booking System Web API Setup and Dev Guide

Change directory and follow instructions in the ReadMe
```shell
cd MvcBookingSystemApi
```

## Contributors
- [Governor Baloyi](https://bitbucket.org/governorb)
- [Mohammed Shaikjee](https://bitbucket.org/shaikjee_m)
- [Siddeeq Laher](http://bitbucket.org/siddeeqlaher)
- [Thabo Ramaano](http://bitbucket.org/)
- [Omer Elgoni](http://bitbucket.org/)

## Dev Guide and Resources

1. See [Trello](https://trello.com/b/70pZpGsu/c-project) for our backlog, use bbd email address
2. See To Do Cards if not Doing any task
3. Create a feature branch and checkout
    ```
    git checkout -b <branch_name>
    ```
4. Make changes then Git add files
    ```
    git add <filename>
    ```
5. Commit message should have ticket number, for example:
    ```
    git commit -m "<Ticket_No>: Added User Controller and Model"
    ```
6. Push changes of your branch
    ```
    git push
    ```
7. Make sure your changes are pushed, then create a Pull Request (PR). Once approved, merge into master.
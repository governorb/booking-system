# Booking System FrontEnd

This is the FrontEnd MVC Project. It consumes the backend [MvcBookingSystemApi](../MvcBookingSystemApi). This approach implements BackEnd separate from the FrontEnd as Projects.

## How to setup?

To setup the enviromnent
```shell
dotnet tool install --global dotnet-ef
dotnet tool install --global dotnet-aspnet-codegenerator
dotnet add package Microsoft.EntityFrameworkCore.SQLite
dotnet add package Microsoft.AspNetCore.Diagnostics.EntityFrameworkCore
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.Extensions.Logging.Debug
```
The following adds a Controller, with corresponding View and Links the context of the Model Specified.

Take note: You may skip the following command...

| Tag | Value | Description |
|:---:|:---:|:---:|
| -name | UsersController | Specifies the name of the controller to be created |
| -m | User | Specifies the name of the model to be used for the creation |
| -dc | MvcBookingSystemContext | Specifies the Database Context to be used |
| --relativeFolderPath | Controllers | I think its the folder in which the contoller will be created on |
| --useDefaultLayout | | I am not sure [Anyone can edit] |
| --referenceScriptLibraries | | I am not sure [Anyone can edit] |

*You may skip this step, I think...* (For Devs Only...)
```shell
dotnet aspnet-codegenerator controller -name UsersController -m User -dc MvcBookingSystemContext --relativeFolderPath Controllers --useDefaultLayout --referenceScriptLibraries
```
The following configures the database, I think...
```shell
dotnet ef migrations add InitialCreate
dotnet ef database update
```

To run the application
```
dotnet run
```
_Check the ports your application is listening..._
using System.Collections.Generic;
using System.Threading.Tasks;
using MvcBookingSystem.Models;
using System.Net.Http;
using Newtonsoft.Json;

namespace MvcBookingSystem.Services
{
    public class UserService: IUserService
    {
        private readonly HttpClient _httpClient;

        public UserService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<List<User>> GetUsers()
        {
            var responseString = await _httpClient.GetStringAsync("api/Users");

            var users = JsonConvert.DeserializeObject<List<User>>(responseString);
            
            return users;
        }
    }
}
using System.Threading.Tasks;
using System.Collections.Generic;
using MvcBookingSystem.Models;

namespace MvcBookingSystem.Services
{
    public interface IUserService
    {
        Task<List<User>> GetUsers();
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MvcBookingSystemApi.Models;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Configuration;

namespace MvcBookingSystemApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private readonly HttpClient client = new HttpClient();
        private readonly ApplicationContext _context;
        private readonly IConfiguration _config;
        

        public WeatherController(IConfiguration config, ApplicationContext context)
        {
            _config = config;
            _context = context;
            client.BaseAddress = new Uri("http://api.openweathermap.org/data/2.5/forecast/");
            
        }

        // GET: api/Weather/Johannesburg
        [HttpGet]
        public async Task<String> GetForecast(string city)
        {
            HttpResponseMessage response = await client.GetAsync("?q="+city+"&units=metric&appid=" + _config["ExternalAPI:WeatherApi"]);
            response.EnsureSuccessStatusCode();
            string jsonString = await response.Content.ReadAsStringAsync();
            dynamic jsonObject = JsonSerializer.Deserialize<dynamic>(jsonString);
            return JsonSerializer.Serialize(jsonObject);
        }

    }
    
}

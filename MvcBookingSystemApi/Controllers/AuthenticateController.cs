using System;
using System.Text;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MvcBookingSystemApi.Models;
using System.Security.Claims;
using System.Linq;
using BC = BCrypt.Net.BCrypt;
using MvcBookingSystemApi.Services;

namespace MvcBookingSystemApi.Controllers
{
    public class AuthenticateController : Controller
    {
        private IConfiguration _config;
        private readonly ApplicationContext _context;
        private IMyEmailService _mailService;

        public AuthenticateController(IConfiguration config, ApplicationContext context, IMyEmailService mailService)
        {
            _config = config;
            _context = context;
            _mailService = mailService;
        }

        private string GenerateJSONWebToken(AuthModel authModel)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>()
            {
                new Claim("Id", authModel.Id.ToString()),
                new Claim(ClaimTypes.Email, authModel.Email),
                new Claim("IsActive", authModel.IsActive? "True": "False"),
                new Claim("EmailActive", authModel.EmailActive? "True": "False")
            };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(5),
              signingCredentials: credentials);

            string strToken = new JwtSecurityTokenHandler().WriteToken(token);

            return strToken;
        }

        private async Task<AuthModel> AuthenticateUser(LoginModel login)
        {
            if (!UserExists(login.Email))
            {
                return null;
            } 
            else
            {
                var u = _context.User.Where(u => u.Email == login.Email).FirstOrDefault();

                if (BC.Verify(login.Password, u.PasswordHash))
                {
                    var authModel = new AuthModel()
                    {
                        Id = (int)u.Id,
                        Email = u.Email,
                        IsActive = u.IsActive
                    };

                    return await Task.Run(() => authModel);
                }

                return null;
            }
        }

        [AllowAnonymous]
        [HttpPost(nameof(Register))]
        public async Task<IActionResult> Register([FromBody] RegisterModel data)
        {
            if (data.Password != data.ConfirmPassword)
            {
                return BadRequest(new { Message = "Passwords do not match" });
            }
            else if (UserExists(data.Email))
            {
                return BadRequest(new { Message = "User already exists" });
            }
            else
            {
                var user = new User()
                {
                    Email = data.Email,
                    PasswordHash = BC.HashPassword(data.Password)
                };

                _context.User.Add(user);
                await _context.SaveChangesAsync();

                _mailService.SendVerificationEmail(user.Email, user.ActivationCode.ToString());

                return CreatedAtAction("GetUser", new { Controller = "Users", id = user.Id }, 
                    new { Message = "User registered successfully" });
            }
        }

        [AllowAnonymous]
        [HttpPost("/ForgotPassword")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordModel data)
        {
            if (data.Email != data.ConfirmEmail)
            {
                return BadRequest(new { Message = "Email do not match" });
            }
            else if (!UserExists(data.Email))
            {
                return NotFound(new { Message = "User does not exits" });
            }
            else
            {
                var user = _context.User.Where(u => u.Email == data.Email).FirstOrDefault();

                user.ResetPasswordCode = Guid.NewGuid();
                await _context.SaveChangesAsync();

                _mailService.SendForgotPasswordLink(user.Email, user.ResetPasswordCode.ToString());

                return Ok(new { Message = "Success, please check your email for instructions" });
            }
        }

        [AllowAnonymous]
        [HttpPost("/ResetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordModel data)
        {
            if (data.Password != data.ConfirmPassword)
            {
                return BadRequest(new { Message = "Passwords do not match" });
            }
            else if (!UserExists(data.Email))
            {
                return NotFound(new { Message = "User does not exists" });
            }
            else
            {
                var user = _context.User.Where(u => u.Email == data.Email).FirstOrDefault();

                if (user.ResetPasswordCode != data.Token)
                {
                    return BadRequest(new { Message = "Token is invalid" });
                }

                user.PasswordHash = BC.HashPassword(data.Password);
                user.ResetPasswordCode = new Guid();

                await _context.SaveChangesAsync();

                return Ok(new { Message = "Password reset successfully. Please login using your new password" });
            }
        }

        private bool UserExists(string email)
        {
        return _context.User.Any(user => user.Email == email);
        }


        [AllowAnonymous]
        [HttpGet("Register/User/Verification")]
        public async Task<IActionResult> EmailVerification([FromQuery(Name = "code")] String code)
        {
            var user = _context.User.Where(u => u.ActivationCode == new Guid(code)).FirstOrDefault();

            if (user != null)
            {
                user.EmailVerification = true;
                user.ActivationCode = new Guid();

                await _context.SaveChangesAsync();

                return Ok(new { Message = "User Email Confirmed" });
            }

            return BadRequest(new { Message = "Please try again later or contact system admin" });
        }

        [AllowAnonymous]
        [HttpPost(nameof(Login))]
        public async Task<IActionResult> Login([FromBody] LoginModel data)
        {
            var user = await AuthenticateUser(data);

            if (user != null)
            {
                var tokenString = GenerateJSONWebToken(user);
                return Ok(new { Token = tokenString, Message = "Success" });
            }

            return Unauthorized(new { Message = "Incorrect username or password" });
        }

        [HttpGet(nameof(Get))]
        public async Task<IEnumerable<string>> Get()
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");

            return new string[] { accessToken };
        }
    }
}
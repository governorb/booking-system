using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MvcBookingSystemApi.Models;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;

namespace MvcBookingSystemApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ApplicationContext _context;

        public UsersController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: api/Users
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUser()
        {
            var currentUser = HttpContext.User;

            if (currentUser.HasClaim(c => c.Type == "Email"))
            {
                Console.WriteLine("Current Email:");
                Console.WriteLine(currentUser.Claims.FirstOrDefault(c => c.Type == "Email").Value);
            }

            return await _context.User.ToListAsync();
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(long id)
        {
            var user = await _context.User.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        [Authorize]
        [HttpGet("MyDetails")]
        public async Task<IActionResult> GetDetails()
        {
            var currentUser = HttpContext.User;

            if (currentUser.HasClaim(c => c.Type == "Id"))
            {
                var id = currentUser.Claims.FirstOrDefault(c => c.Type == "Id").Value;

                var user = await _context.User.FindAsync((long)Int64.Parse(id));

                if (user == null)
                {
                    return NotFound();
                }

                return Ok(user);
            }

            return BadRequest();

        }

        [Authorize]
        [HttpPut("api/Users/Update")]
        public async Task<IActionResult> UpdateUser(UpdateUserModel data)
        {
            var currentUser = HttpContext.User;

            if (currentUser.HasClaim(c => c.Type == "Id"))
            {

                var id = Int64.Parse(currentUser.Claims.FirstOrDefault(c => c.Type == "Id").Value);

                var user = _context.User.FindAsync(id).Result;

                user.FirstName = data.FirstName;
                user.LastName = data.LastName;
                user.Cellphone = data.Cellphone;

                _context.Entry(user).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(id))
                    {
                        return NotFound(new { Message = "User does not exists" });
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            return NoContent();
        }

        [Authorize]
        [HttpGet("api/Users/MyEvents")]
        public async Task<ActionResult<List<Event>>> MyEvents()
        {
            // Get user id
            // return events linked to the user

            var currentUser = HttpContext.User;

            long currentUserId = Int64.Parse(currentUser.Claims.FirstOrDefault(c => c.Type == "Id").Value);

            if (_context.EventUser.Count() > 0)
            {              
                var registrationsQuery =
                from eventUser in _context.EventUser
                where eventUser.UserId == currentUserId && eventUser.userCancelled == false
                select eventUser.Event;

                if (registrationsQuery.Count() > 0)
                {
                    return registrationsQuery.ToList();
                }
                else
                {
                    return BadRequest("User is not registered to any events");
                }
            }

            return new List<Event>();
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(long id, User user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(User user)
        {
            _context.User.Add(user);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUser", new { id = user.Id }, user);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(long id)
        {
            var user = await _context.User.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            _context.User.Remove(user);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool UserExists(long id)
        {
            return _context.User.Any(e => e.Id == id);
        }
    }
}

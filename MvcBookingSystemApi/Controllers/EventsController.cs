using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MvcBookingSystemApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

namespace MvcBookingSystemApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly ApplicationContext _context;
        private readonly IConfiguration _config;
        static HttpClientHandler handler = new HttpClientHandler();
        HttpClient client = new HttpClient(handler);



        public EventsController(IConfiguration config, ApplicationContext context)
        {
            _config = config;
            _context = context;

            client.BaseAddress = new Uri("http://open.mapquestapi.com/geocoding/v1/address");
        }

        // GET: api/Events
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Event>>> GetEvent()
        {
            // Send api call to weather
            // Send the event back with weather
            return await _context.Event.ToListAsync();
        }

        // GET: api/Events/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Event>> GetEvent(long id)
        {
            var @event = await _context.Event.FindAsync(id);

            if (@event == null)
            {
                return NotFound();
            }

            return @event;
        }

        // POST: api/Events/5/Book
        [Authorize]
        [HttpPost("api/Events/{id}/Book")]
        public async Task<IActionResult> BookEvent(long id)
        {
            // Find event
            var @event = await _context.Event.FindAsync(id);
            if (@event == null)
            {
                return NotFound();
            }

            // Get User Id from HttpContext - Access Token
            var currentUser = HttpContext.User;
            // Make sure user is active
            // TODO:
            if (currentUser.HasClaim(c => c.Type == "Id"))
            {
                long currentUserId = Int64.Parse(currentUser.Claims.FirstOrDefault(c => c.Type == "Id").Value);
                User currentUserObj = _context.User.Find(currentUserId);
                // Link the two
                if (_context.EventUser.Count() > 0)
                {
                    var query = _context.EventUser.Where(eventUser => eventUser.EventId == @event.Id && eventUser.UserId == currentUserId);
                    // if the eventUser exists then we simply set userCancelled to false
                    if (query.Count() == 1)
                    {
                        var eventUser = query.First();
                        if (eventUser.userCancelled)
                        {
                            eventUser.userCancelled = false;
                            await _context.SaveChangesAsync();
                            return Ok("User registered for event");
                        }
                        else
                        {
                            return BadRequest("User is already registered for event");
                        }
                    }
                    else
                    {
                        // otherwise we make a new one and populate it
                        EventUser eventUser = new EventUser();
                        eventUser.EventId = @event.Id;
                        eventUser.Event = @event;
                        eventUser.UserId = currentUserId;
                        eventUser.User = currentUserObj;
                        eventUser.userCancelled = false;
                        _context.EventUser.Add(eventUser);
                        await _context.SaveChangesAsync();
                        // @event.Attendees.Add(eventUser);
                        // currentUserObj.Events.Add(eventUser);
                        return Ok("User registered for event");
                    }
                    // Send an email to the user
                }
            }
            else
            {
                throw new Exception("User does not exist");
            }

            return Ok();
        }

        [Authorize]
        [HttpPost("api/Events/{id}/UnBook")]
        public async Task<IActionResult> UnBook(long id)
        {

            var currentUser = HttpContext.User;
            long userId = Int64.Parse(currentUser.Claims.FirstOrDefault(c => c.Type == "Id").Value);

            if (!EventExists(id))
            {
                return NotFound();
            }
            else
            {

                var currentEU = _context.EventUser.Where(EU => (EU.UserId == userId && EU.EventId == id)).FirstOrDefault();
                currentEU.userCancelled = true;
                _context.SaveChanges();
                return Ok();
            }
        }

        [Authorize]
        [HttpGet("api/Events/{id}/Attendees")]
        public async Task<ActionResult<List<User>>> GetActiveAttendees(long id)
        {
            var @event = await _context.Event.FindAsync(id);
            if (@event == null)
            {
                return NotFound();
            }

            if (_context.EventUser.Count() > 0)
            {
                var attendeesQuery =
                from eventUser in _context.EventUser
                where eventUser.EventId == @event.Id && !eventUser.userCancelled
                select eventUser.User;


                return attendeesQuery.ToList();
            }

            return new List<User>();

        }

        // PUT: api/Events/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEvent(long id, Event @event)
        {
            if (id != @event.Id)
            {
                return BadRequest();
            }

            _context.Entry(@event).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Events
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<Event>> PostEvent(Event @event)
        {
            var currentUser = HttpContext.User;

            string api_key = _config["ExternalAPI:GoogleGeoCodeApi"];

            HttpResponseMessage response = await client.GetAsync("?key=" + api_key + "&maxResults=1" + "&location=" + (string.IsNullOrEmpty(@event.Location.addressLine1) ? "" : @event.Location.addressLine1 + ",") + (string.IsNullOrEmpty(@event.Location.addressLine2) ? "" : @event.Location.addressLine2 + ",") + (string.IsNullOrEmpty(@event.Location.addressLine3) ? "" : @event.Location.addressLine3 + ",") + @event.Location.city);
            response.EnsureSuccessStatusCode();
            string jsonString = await response.Content.ReadAsStringAsync();

            dynamic jsonObject = JsonConvert.DeserializeObject<dynamic>(jsonString);

            long id = Int64.Parse(currentUser.Claims.FirstOrDefault(c => c.Type == "Id").Value);

            @event.OrganizerId = id;
            @event.Attendees = new List<EventUser>();
            @event.Location.lat = jsonObject.results[0].locations[0].latLng.lat;
            @event.Location.lng = jsonObject.results[0].locations[0].latLng.lng;

            _context.Event.Add(@event);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEvent", new { id = @event.Id }, @event);
        }

        // PUT: api/Events/5
        [Authorize]
        [HttpPut("api/Events/{id}/Cancel")]
        public async Task<IActionResult> CancelEvent(long id)
        {
            var currentUser = HttpContext.User;

            long userId = Int64.Parse(currentUser.Claims.FirstOrDefault(c => c.Type == "Id").Value);

            var @event = _context.Event.Where(e => e.OrganizerId == userId && e.Id == id).FirstOrDefault();

            @event.isCancel = true;
           _context.Entry(@event).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventOrganizerIsValid(userId, id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool EventOrganizerIsValid(long userId, long eventId)
        {
            return _context.Event.Any(e => e.Id == eventId && e.OrganizerId == userId);
        }

        private bool EventExists(long id)
        {
            return _context.Event.Any(e => e.Id == id);
        }
    }
}

using Microsoft.AspNetCore.Mvc;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;

namespace MvcBookingSystemApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
    public interface IEmailService
    {
        void SendEmail(string from, string to, string subject, string html,string authenticateEmailAdress,string authenticatePwd);
    }

    public class EmailService : IEmailService
    {
      
        public void SendEmail(string from, string to, string subject, string html,string authenticateEmailAdress,string authenticatePwd)
        {
            // create message
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(from));
            email.To.Add(MailboxAddress.Parse(to));
            email.Subject = subject;
            email.Body = new TextPart(TextFormat.Html) { Text = html };

            // send email
            using var smtp = new SmtpClient();
            smtp.Connect("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
            smtp.Authenticate(authenticateEmailAdress,authenticatePwd);
            smtp.Send(email);
            smtp.Disconnect(true);
        }
    }
}}

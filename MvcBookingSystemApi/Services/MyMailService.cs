using MimeKit;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using System;

namespace MvcBookingSystemApi.Services
{
    public class MyMailService : IMyEmailService
    {
        public readonly IConfiguration _config;
        public MyMailService(IConfiguration config)
        {
            _config = config;
        }
        public bool SendEmail(string to, string subject, string html, string authenticationAddress, string authenticatePwd)
        {
            bool sent = false;
            // Create message
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(_config["Credentials:Email:Username"]));
            email.To.Add(MailboxAddress.Parse(to));
            email.Subject = subject;
            email.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = html
            };

            // Send email
            using (var smtp = new SmtpClient())
            {
                smtp.Connect(_config["Credentials:Email:Host"], Int32.Parse(_config["Credentials:Email:Port"]),
                    MailKit.Security.SecureSocketOptions.StartTls);
                smtp.Authenticate(_config["Credentials:Email:Username"], _config["Credentials:Email:Password"]);
                smtp.Send(email);
                smtp.Disconnect(true);

                sent = true;
            }

            return sent;
        }

        public void SendForgotPasswordLink(string to, string activationCode)
        {
            var subject = "Booking System Email Activation";
            var body = "<h1 style='color: #157DEC'>Booking System Reset Password</h1>" + 
            "<p style='color: #157DEC'>You have requested to change your password.</p>" +
            $"<p style='color: #157DEC'>Please use this token: {activationCode}</p>";
            SendEmail(to, subject, body, _config["Credentials:Email:Username"], _config["Credentials:Email:Password"]);
        }

        public void SendVerificationEmail(string to, string activationCode)
        {
            var generateUserVerificationLink = $"https://localhost:8081/Register/User/Verification/?code={activationCode}";

            var subject = "Booking System Email Activation";
            var body = "<h1 style='color: #157DEC'>Booking System</h1>" + 
            "<p style='color: #157DEC'>Your registration completed successfully.</p>" +
            "<p style='color: #157DEC'>Please click <a href=" +
            generateUserVerificationLink + ">" + "here" + "</a>";
            SendEmail(to, subject, body, _config["Credentials:Email:Username"], _config["Credentials:Email:Password"]);
        }
    }
}
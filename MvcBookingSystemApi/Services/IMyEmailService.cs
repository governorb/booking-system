namespace MvcBookingSystemApi.Services
{
    public interface IMyEmailService
    {
        bool SendEmail(string to, string subject, string html, string authenticationAddress, string authenticatePwd);
        void SendVerificationEmail(string to, string activationCode);
        void SendForgotPasswordLink(string to, string activationCode);
    }
}
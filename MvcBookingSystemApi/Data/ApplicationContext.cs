using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MvcBookingSystemApi.Models;

    public class ApplicationContext : DbContext
    {
        public ApplicationContext (DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<EventUser>()
            .HasKey(t => new { t.UserId, t.EventId });

        modelBuilder.Entity<EventUser>()
            .HasOne(pt => pt.Event)
            .WithMany(p => p.Attendees)
            .HasForeignKey(pt => pt.EventId);

        modelBuilder.Entity<EventUser>()
            .HasOne(pt => pt.User)
            .WithMany(t => t.Events)
            .HasForeignKey(pt => pt.UserId);
    }

        public DbSet<MvcBookingSystemApi.Models.User> User { get; set; }

        public DbSet<MvcBookingSystemApi.Models.Event> Event { get; set; }

        public DbSet<MvcBookingSystemApi.Models.Location> Location { get; set; }

        public DbSet<MvcBookingSystemApi.Models.EventUser> EventUser { get; set; }
    }

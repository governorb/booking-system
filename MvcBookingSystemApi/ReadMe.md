# MVC Booking System API

## How to setup?

To setup the environment
```shell
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.EntityFrameworkCore.InMemory
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.AspNetCore.Mvc.NewtonsoftJson --version 5.0.0
dotnet tool install -g dotnet-aspnet-codegenerator
```

The following configures the database
```shell
dotnet ef migrations add <Snapshot>
dotnet ef database update
```
You need to configure environment variables in [appsettings.json](appsettings.json) or use Safe storage of app secrets.
The following instructions configures env. _Make sure you provide a valid ConnectionString, SecretKey, Issuer, Openweather API Key, and GoogleGeoCoding API Key_
```shell
dotnet user-secrets init

dotnet user-secrets set "ConnectionStrings:ApplicationContext" "<ConnectionString>"
dotnet user-secrets set "Jwt:Key" "<SecretKey>"
dotnet user-secrets set "Jwt:Issuer" "<Issuer>"
dotnet user-secrets set "ExternalAPI:WeatherApi" "<WeatherApi_Key>"
dotnet user-secrets set "ExternalAPI:GoogleGeoCodeApi" "<GoogleGeoCodeApi_Key>"
```
To run the project...
```shell
dotnet run
```
Navigate to localhost:{Port}/swagger, _see swagger page_

## Dev Guide

To add a controller, use this command
```shell
dotnet aspnet-codegenerator controller -name UsersController -async -api -m User -dc UserContext -outDir Controllers
```
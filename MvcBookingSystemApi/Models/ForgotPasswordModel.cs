namespace MvcBookingSystemApi.Models
{
    public class ForgotPasswordModel
    {
        public string Email { get; set; }
        public string ConfirmEmail { get; set; }
    }
}
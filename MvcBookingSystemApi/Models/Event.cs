using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace MvcBookingSystemApi.Models
{
  public class Event
  {
    public long Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public DateTime InitialDate { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public long LocationId { get; set; }
    public Location Location { get; set; }
    [JsonIgnore]
    public bool isCancel { get; set; } = false;
    public long OrganizerId { get; set; }
    [JsonIgnore]
    public List<EventUser> Attendees { get; set;}
  }
}
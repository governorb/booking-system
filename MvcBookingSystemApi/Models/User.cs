using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using System.Collections.Generic;

namespace MvcBookingSystemApi.Models
{
    public class User
    {
        [Required]
        public long Id { get; set; }
        [MinLength(3)]
        public string FirstName { get; set; }
        [MinLength(3)]
        public string LastName { get; set; }
        [MinLength(3)]
        public string Email { get; set; }
        [JsonIgnore]
        public Guid ActivationCode { get; set; } = Guid.NewGuid();
        public Guid ResetPasswordCode { get; set; }
        public bool EmailVerification { get; set; } = false;
        public DateTime RegistrationDate { get; set; } = DateTime.Now;
        [MinLength(3)]
        public string Cellphone { get; set; }
        [MinLength(3)]
        [JsonIgnore]
        public string Token { get; set; }
        [MinLength(8)]
        [JsonIgnore]
        public string PasswordHash { get; set; }
        public bool IsActive { get; set; } = true;
        public List<EventUser> Events { get; set; }
    }
}
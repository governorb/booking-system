namespace MvcBookingSystemApi.Models
{
    public class EventUser
    {
        public long EventId { get; set; }
        public Event Event { get; set; }

        public long UserId { get; set; }
        public User User { get; set; }

        public bool userCancelled { get; set;}
    }
}
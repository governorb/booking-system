using System.ComponentModel.DataAnnotations;

namespace MvcBookingSystemApi.Models
{
    public class UpdateUserModel
    {
        [MinLength(3)]
        public string FirstName { get; set; }
        [MinLength(3)]
        public string LastName { get; set; }
        [MinLength(3)]
        public string Cellphone { get; set; }
    }
}
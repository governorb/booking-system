namespace MvcBookingSystemApi.Models
{
    public class AuthModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public bool EmailActive { get; set; }
        public bool IsActive { get; set; }
    }
}
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MvcBookingSystemApi.Models
{
  public class Location
  {
    public long Id { get; set; }

    public string addressLine1 { get; set; }

    public string addressLine2 { get; set; }

    public string addressLine3 { get; set; }

    [Required]
    public string city { get; set; }

    public string postalCode { get; set; }

    public double lat { get; set; }

    public double lng { get; set; }
    public List<Event> Events { get; set; }
  }
}